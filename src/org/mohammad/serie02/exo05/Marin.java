package org.mohammad.serie02.exo05;

public class Marin implements Comparable<Marin>
{
	private String nom;
	private String prenom;
	private int salaire;
	
	// 1/ Premier constructeur
	public Marin(String nouveauNom, String nouveauPrenom, int nouveauSalaire) 
	{
		nom = nouveauNom;
		prenom = nouveauPrenom;
		salaire = nouveauSalaire;
	}
	
	// 2/ Deuxi�me constructeur
	public Marin(String nouveauNom, int nouveauSalaire)
	{
		nom = nouveauNom;
		prenom = "";
		salaire = nouveauSalaire;
	}
	
	// 3/ M�thode setNom() "� la main"
	public void setNom(String nouveauNom)
	{
		this.nom = nouveauNom;
	}
	
	// M�thode getNom() "� la main"
	public String getNom()
	{
		return nom;
	}

	// 4/ Getter et Setter avec "Alt+Shift+S" --> Generate Getters and Setters
	public String getPrenom() 
	{
		return prenom;
	}

	public void setPrenom(String prenom) 
	{
		this.prenom = prenom;
	}

	public int getSalaire() 
	{
		return salaire;
	}

	public void setSalaire(int salaire) 
	{
		this.salaire = salaire;
	}
	
	// 5/ M�thode augmenteSalaire
	public void augmenteSalaire(int augmentation)
	{
		salaire = salaire + augmentation;
	}
	
	// 7/ toString()
	@Override
	public String toString() 
	{
		return "Marin [nom=" + nom + ", prenom=" + prenom + ", salaire=" + salaire + "]";
	}

	// 8/ hCode()
	@Override
	public int hashCode() 
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nom == null) ? 0 : nom.hashCode());
		result = prime * result + ((prenom == null) ? 0 : prenom.hashCode());
		result = prime * result + salaire;
		return result;
	}

	// equals()
	@Override
	public boolean equals(Object obj) 
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Marin other = (Marin) obj;
		if (nom == null) {
			if (other.nom != null)
				return false;
		} else if (!nom.equals(other.nom))
			return false;
		if (prenom == null) {
			if (other.prenom != null)
				return false;
		} else if (!prenom.equals(other.prenom))
			return false;
		if (salaire != other.salaire)
			return false;
		return true;
	}
	
	// 11/ Implementaion de Comparable 
	@Override
	public int compareTo(Marin marin) 
	{
		int compareNom;
		int comparePrenom;
		
		compareNom = this.nom.compareTo(marin.nom);
		if((compareNom < 0) || (compareNom > 0))
		{
			return compareNom;
		}
		
		comparePrenom = this.prenom.compareTo(marin.prenom);
		if((comparePrenom < 0) || (comparePrenom > 0))
		{
			return comparePrenom;
		}
		// TODO Auto-generated method stub
		return 0;
	}
}