package org.mohammad.serie02.exo05;

public class Main 
{
	public static void main(String[] args)
		{	
			// 6/ Creation de 3 objets Marin
			Marin m1 = new Marin("Mohammad", "Akash",10500);
			Marin m2 = new Marin("Mohammad", "Akash",10500);
			Marin m3 = new Marin("Khan", "Bilal",2);
			
			// 7/ Afficher directement les marins avec System.out.println
			System.out.println("Affichage des champs de la classe Marin avec println :");
			System.out.println(m1.toString());
			System.out.println(m2.toString());
			System.out.println(m3.toString());
			System.out.println("");
			
			// 8/ M�thode de comparaison de deux instances Marin
			System.out.println("Comparaison de deux instances Marin avec equals() :");
			if(m1.equals(m2))
			{
				System.out.println("m1 et m2 sont identiques");
			}
			else
			{
				System.out.println("m1 et m2 ne sont pas identiques");
			}
			System.out.println("");
			
			// 9/ Modification de m2
			m2 = m1;
			
			// 10/ Afficher les codes de hachages
			System.out.println("Codes de hachage des 3 marins");
			int code_hachage_m1 = m1.hashCode();
			int code_hachage_m2 = m2.hashCode();
			int code_hachage_m3 = m3.hashCode();

			System.out.println("m1 : " + code_hachage_m1);
			System.out.println("m2 : " + code_hachage_m2);
			System.out.println("m3 : " + code_hachage_m3);	
			System.out.println("");
			
			// 11/ Comparaison entre m1 et m2 qui sont �gaux
			int comparaison = m1.compareTo(m2);
			System.out.println("Comparaison de m1 et m2 par ordre alphab�tique des noms puis pr�nom :");
			if(comparaison > 0)
				System.out.println("m1 > m2");
			if(comparaison < 0)
				System.out.println("m1 < m2");
			if(comparaison == 0)
				System.out.println("m1 = m2");
			
			// Comparaison entre m1 et m3 (Nom de m1 : Mohammad - Nom de m3 : Khan)
			comparaison = m1.compareTo(m3);
			System.out.println("Comparaison de m1 et m3 par ordre alphab�tique des noms puis pr�nom :");
			if(comparaison > 0)
				System.out.println("m1 > m3");
			if(comparaison < 0)
				System.out.println("m1 < m3");
			if(comparaison == 0)
				System.out.println("m1 = m3");
			
			// Comparaison entre m1 et m4 (Nom m1 et m4: Mohammad et Pr�nom m3 : Akash - Pr�nom m4 : Tanuja)
			Marin m4 = new Marin("Mohammad", "Tanuja",10500);
			comparaison = m1.compareTo(m4);
			System.out.println("Comparaison de m1 et m4 par ordre alphab�tique des noms puis pr�nom :");
			if(comparaison > 0)
				System.out.println("m1 > m4");
			if(comparaison < 0)
				System.out.println("m1 < m4");
			if(comparaison == 0)
				System.out.println("m1 = m4");
		}
}