package org.mohammad.serie02.exo06;

import java.util.*;

public class MarinUtil
{
	// 1/ M�thode augmenteSalaire
	public void augmenteSalaire(Marin[] marins, int pourcentage)
	{
		int montant = 0;
		int longueur = marins.length;
		for(int i = 0 ; i < longueur ; i++)
		{
			montant = marins[i].getSalaire();
			montant = montant + ((montant * pourcentage) / 100);
			marins[i].setSalaire(montant);
			montant = 0;
		}
	}
	
	// 2/ M�thode getMaxSalaire
	public int getMaxSalaire(Marin[] marins)
	{
		int salaire;
		int longueur = marins.length;
		
		salaire = marins[0].getSalaire();
		
		for(int i = 1 ; i < longueur ; i++)
		{
			if(marins[i].getSalaire() > salaire)
			{
				salaire = marins[i].getSalaire();
			}
		}
		
		return salaire;
	}
	
	// 3/ M�thode getMoyenneSalaire
	public int getMoyenneSalaire(Marin[] marins)
	{
		int salaireMoy = 0;
		int longueur = marins.length;
		
		for(int i = 0 ; i < longueur ; i++)
		{
			salaireMoy = salaireMoy + marins[i].getSalaire();
		}
		salaireMoy = salaireMoy / longueur;
		
		return salaireMoy;
	}
	
	// 4/ M�thode getMedianeSalaire
	public int getMedianeSalaire(Marin[] marins)
	{
		int salaireMed = 0;
		int longueur = marins.length;
		int tab[] = new int[longueur];
		
		for(int i = 0 ; i < longueur ; i++)
		{
			tab[i] = marins[i].getSalaire();
		}
		
		Arrays.sort(tab);
		
		if(longueur % 2 != 0)
		{
			salaireMed = tab[(longueur - 1) / 2 ];
		}
		else
		{
			salaireMed = (tab[(longueur / 2)] + tab[(longueur - 2) / 2]) / 2; 
		}
		
		return salaireMed;
	}
}