package org.mohammad.serie02.exo06;

public class Main 
{
	public static void main(String[] args)
	{
		// D�claration d'un tableau de Marin
		Marin[] marins = new Marin[5];
		MarinUtil salaire = new MarinUtil();
		
		marins[0] = new Marin("Mohammad","Akash",200000);
		marins[1] = new Marin("Rafaqat","Amrose",1000);
		marins[2] = new Marin("Khan","Bilal",2);
		marins[3] = new Marin("Khan","Shahrukh",5000000);
		marins[4] = new Marin("Trump","Donald",1);
		
		int longueur = marins.length;
		int maxSalaire = 0;
		int moySalaire = 0;
		int medSalaire = 0;
		
		// 1/ Augmentation de 30% de chaque salaire
		salaire.augmenteSalaire(marins, 30);
		System.out.println("Salaire avec une augmentation de 30% : ");
		for(int i = 0 ; i < longueur ; i++)
		{
			System.out.println(marins[i].getPrenom() + " : " + marins[i].getSalaire());
		}
		System.out.println("");
		
		// 2/ Affichage du salaire du marin le mieux pay�
		maxSalaire = salaire.getMaxSalaire(marins);
		System.out.println("Le salaire max est : " + maxSalaire);
		System.out.println("");
		
		// 3/ Affichage du salaire moyen des marins
		moySalaire = salaire.getMoyenneSalaire(marins);
		System.out.println("Le salaire moyen est : " + moySalaire);
		System.out.println("");
		
		// 4/ Affichage du salaire m�dian des marins
		medSalaire = salaire.getMedianeSalaire(marins);
		System.out.println("Le salaire m�dian est : " + medSalaire);
		System.out.println("");
	}
}
