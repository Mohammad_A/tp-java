package org.mohammad.serie01.exo02;

import java.math.BigInteger;				// Importation d'une librairie "BigInteger puis Ctrl+Espace"


public class Factorielle 
{
	// Cas o� nbr est un int
		public int factorielle1(int nbr)	// Cr�ation de la m�thode factorielle
		{
			int fact = 1;
			for(int i = 1;i<=nbr;i++)
			{
				fact = fact*i;
			}
			
			return fact;
		}
		
		// Cas o� nbr est un double
		public double factorielle2(double nbr)
		{
			double fact = 1;
			for(int i = 1;i<=nbr;i++)
			{
				fact = fact*i;
			}
			
			return fact;
		}
		
		// Cas o� nbr est un BigInteger(Objet)
		public BigInteger factorielle3(BigInteger nbr)
		{
			BigInteger i = new BigInteger("1");
			BigInteger fact = new BigInteger("1");
			// Utiliser les m�thodes de BigInteger (BigInteger.)
			for(i = BigInteger.valueOf(1);i.compareTo(nbr)<=0;i = i.add(BigInteger.ONE))
			{
				fact = fact.multiply(i);
			}
			
			return fact;
		}
}
