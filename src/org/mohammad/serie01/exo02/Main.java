package org.mohammad.serie01.exo02;

import java.math.BigInteger;							// On importe BigInteger

public class Main 
{
	public static void main(String[] args)				// M�thode main
	{
		BigInteger nbr = new BigInteger("10");			// factorielle3 prend en argument un BigInteger
		Factorielle fact = new Factorielle();
		
		//Factorielle de 10 avec les 3 types (int, double, BigInteger)
		System.out.println("La factorielle de 10 est :" + fact.factorielle1(10)) ;
		System.out.println("La factorielle de 10 est :" + fact.factorielle2(10)) ; 
		System.out.println("La factorielle de 10 est :" + fact.factorielle3(nbr)) ;
		
		System.out.println("");
		
		//Factorielle de 13 avec les 3 types (int, double, BigInteger)
		System.out.println("La factorielle de 13 est :" + fact.factorielle1(13)) ;
		System.out.println("La factorielle de 13 est :" + fact.factorielle2(13)) ; 
		nbr = new BigInteger("13");
		System.out.println("La factorielle de 13 est :" + fact.factorielle3(nbr)) ;
		
		System.out.println("");
		
		//Factorielle de 25 avec les 3 types (int, double, BigInteger)
		System.out.println("La factorielle de 25 est :" + fact.factorielle1(25)) ;
		System.out.println("La factorielle de 25 est :" + fact.factorielle2(25)) ; 
		nbr = new BigInteger("25");
		System.out.println("La factorielle de 25 est :" + fact.factorielle3(nbr)) ; 
		
		System.exit(0) ;
	}
}