package org.mohammad.serie01.exo03;

public class Main 										// Classe Main
{
	public static void main(String[] args)				// M�thode main
	{
		Bissextile bis = new Bissextile();
		
		// Ann�e 2017
		if(bis.bissextile(2017)==true)
		{
			System.out.println("2017 est une ann�e bissextile !!");
		}
		else
		{
			System.out.println("2017 n'est pas une ann�e bissextile !!");
		}
		System.out.println("");
		
		// Ann�e 2000
		if(bis.bissextile(2000)==true)
		{
			System.out.println("2000 est une ann�e bissextile !!");
		}
		else
		{
			System.out.println("2000 n'est pas une ann�e bissextile !!");
		}
		System.out.println("");
		
		// Ann�e 1996
		if(bis.bissextile(1996)==true)
		{
			System.out.println("1996 est une ann�e bissextile !!");
		}
		else
		{
			System.out.println("1996 n'est pas une ann�e bissextile !!");
		}
		System.out.println("");
		
		// Ann�e 1901 
		if(bis.bissextile(1901)==true)
		{
			System.out.println("1901 est une ann�e bissextile !!");
		}
		else
		{
			System.out.println("1901 n'est pas une ann�e bissextile !!");
		}
		System.out.println("");
		
		// Ann�e 1900
		if(bis.bissextile(1900)==true)
		{
			System.out.println("1900 est une ann�e bissextile !!");
		}
		else
		{
			System.out.println("1900 n'est pas une ann�e bissextile !!");
		}
		System.out.println("");
		System.exit(0) ;
	}
}