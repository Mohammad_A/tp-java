package org.mohammad.serie01.exo03;

public class Bissextile 											// Classe Bissextile
{
	public boolean bissextile(int annee)							// M�thode bissextile
	{
		boolean bis = false;										// Boolean bis = false par d�faut
		// (((Si l'ann�e est divisible par 4) AND (Non divisible par 100)) OR (Ann�e divisible par 400)) 
		if(((annee%4 == 0) && (annee%100 != 0)) || (annee%400 == 0))
		{
			bis = true;												// Alors bis = true donc ann�e bissextile
		}
		return bis;													// On retourne bis
	}
}
