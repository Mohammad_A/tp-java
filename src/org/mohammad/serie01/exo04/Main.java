package org.mohammad.serie01.exo04;


public class Main 													// Classe Main
{
	public static void main(String[] args)							// M�thode main
	{
		Palindrome pal = new Palindrome();
		
		// Quelques phrases  � tester
		String phrase1 = "Rions noir";
		String phrase2 = "Esope reste ici et se repose";
		String phrase3 = "Elu par cette crapule";
		String phrase4 = "Et la marine va venir a Malte";
		String phrase5 = "Palindrome";
		String phrase6 = "Programme JAVA";
		
		/****** Phrase1 ******/
		if(pal.palindrome(phrase1) == true)
		{
			System.out.println(phrase1 + " : Palindrome !!");
		}
		else
		{
			System.out.println(phrase1 + " : Pas un palindrome !!");
		}
		
		/****** Phrase2 ******/
		if(pal.palindrome(phrase2) == true)
		{
			System.out.println(phrase2 + " : Palindrome !!");
		}
		else
		{
			System.out.println(phrase2 + " : Pas un palindrome !!");
		}
		
		/****** Phrase3 ******/
		if(pal.palindrome(phrase3) == true)
		{
			System.out.println(phrase3 + " : Palindrome !!");
		}
		else
		{
			System.out.println(phrase3 + " : Pas un palindrome !!");
		}
		
		/****** Phrase4 ******/
		if(pal.palindrome(phrase4) == true)
		{
			System.out.println(phrase4 + " : Palindrome !!");
		}
		else
		{
			System.out.println(phrase4 + " : Pas un palindrome !!");
		}
		
		/****** Phrase5 ******/
		if(pal.palindrome(phrase5) == true)
		{
			System.out.println(phrase5 + " : Palindrome !!");
		}
		else
		{
			System.out.println(phrase5 + " : Pas un palindrome !!");
		}
		
		/****** Phrase6 ******/
		if(pal.palindrome(phrase6) == true)
		{
			System.out.println(phrase6 + " : Palindrome !!");
		}
		else
		{
			System.out.println(phrase6 + " : Pas un palindrome !!");
		}
		
		System.exit(0);
	}
}