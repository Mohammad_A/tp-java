package org.mohammad.serie03.exo04;

public class Main 
{
	public static void main(String[] args)
	{
		boolean bool;
		Equipage equipage1 = new Equipage(5);
		Equipage equipage2 = new Equipage(2);
		Equipage equipageTotal = new Equipage(10);
		
		// Marins de l'equipage1
		Marin m1 = new Marin("Mohammad", "Akash",10500);
		Marin m2 = new Marin("Rafaqat", "Amrose",7550);
		Marin m3 = new Marin("Khan", "Bilal",2);
		Marin m4 = new Marin("Khan", "Shahrukh",5000000);
		Marin m5 = new Marin("Trump", "Donald",0);
		
		// Marins de l'equipage2
		Marin m6 = new Marin("Mohammad", "Akash",10500);
		Marin m7 = new Marin("Mohammad", "Tanuja",10500);
		
		// Ajout des marins
		equipage1.addMarin(m1);
		equipage1.addMarin(m2);
		equipage1.addMarin(m3);
		equipage1.addMarin(m4);
		equipage1.addMarin(m5);
		equipage2.addMarin(m6);
		equipage2.addMarin(m7);
		System.out.println("Ajout des 5 marins dans equipage1:");
		System.out.println(equipage1 + "\n");
		System.out.println("Ajout des 2 marins dans equipage2:");
		System.out.println(equipage2 + "\n");
		
		// Suppression des marins
		bool = equipage1.removeMarin(m5);
		System.out.println("Suppression du marin m5 :");
		System.out.println("Marin m5 removed : " + bool);
		System.out.println(equipage1);		// On a bien enlever m5
		bool = equipage1.removeMarin(m5);
		System.out.println("Re-suppression du marin m5 :");
		System.out.println("Marin m5 removed : " + bool + "\n");
		
		// Test de pr�sence d'un marin dans Equipage
		System.out.println("Test de pr�sence des marins m4 et m5 :");
		bool = equipage1.isMarinPresent(m4);
		System.out.println("Marin m4 isPresent : " + bool);
		
		bool = equipage1.isMarinPresent(m5);
		System.out.println("Marin m5 isPresent : " + bool + "\n");
		
		// Copier le contenu d'un equipage
		equipageTotal.addAllEquipage(equipage1);
		equipageTotal.addAllEquipage(equipage2);
		System.out.println("Affichage du nouvel �quipage (equipage1 + equipage2) : ");
		System.out.println(equipageTotal + "\n");
		
		// Effacer le contenu d'un �quipage
		System.out.println("Contenu de equipage2 avant clear() : ");
		System.out.println(equipage2);
		equipage2.clear();
		System.out.println("Contenu de equipage2 apr�s clear() : ");
		System.out.println(equipage2 + "\n");
		
		// Avoir le nombre de marin d'un �quipage
		System.out.println("Affichage du nombre de marin dans chaque �quipage : ");
		
		int nbrMarin;
		nbrMarin = equipage1.getNombreMarins();
		System.out.println("Nombre de marin dans l'�quipage1 : "  + nbrMarin);
		nbrMarin = equipage2.getNombreMarins();
		System.out.println("Nombre de marin dans l'�quipage2 : "  + nbrMarin);
		nbrMarin = equipageTotal.getNombreMarins();
		System.out.println("Nombre de marin dans l'�quipageTotal : "  + nbrMarin);
		
		// Salaire moyen des marins d'un equipage
		int salaireMoyen;
		salaireMoyen = equipage1.getMoyenneSalaire(); 
		System.out.println("\nAffichage du salaire moyen des marins de chaque equipage : ");
		System.out.println("Equipage1 : " + salaireMoyen + " euros");
		salaireMoyen = equipageTotal.getMoyenneSalaire(); 
		System.out.println("EquipageTotal : " + salaireMoyen + " euros \n");
		
		// Tester le fonctionnement de la limite du nombre de marins
		// On a d�ja 4 marins dans l'equipage qui peut compter au maximum 5
		System.out.println("Marin limit� � 5 pour l'�quipage1 qui en a 4 pour l'instant :");
		equipage1.addMarin(m7);
		System.out.println("Ajout du 5�me marin : ");
		System.out.println(equipage1);
		
		equipage1.addMarin(m7);
		System.out.println("Ajout du 6�me marin : ");
		System.out.println(equipage1);
	}	
}