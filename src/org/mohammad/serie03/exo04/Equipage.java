package org.mohammad.serie03.exo04;

import java.util.*;
//import java.util.ArrayList;
//import java.util.Collection;

public class Equipage 
{
	// Champ pour limiter le nombre de marins dans un equipage
	private int nombreMax;
	
	// Constructeur pour limiter le nombre de marin � 10
	public Equipage(int size) 
	{
		// Pour limiter ...
		nombreMax = size;
	}
	
	// Creation d'une collection (liste) de type Marin
	private Collection<Marin> collectionMarin = new ArrayList<Marin>();
	
	// Ajout d'un marin
	public boolean addMarin(Marin nouveauMarin)
	{
		boolean bool = false;
		int nbrMarin = getNombreMarins();
		if(nbrMarin < nombreMax)
		{
			bool = collectionMarin.add(nouveauMarin);
		}
		
		return bool;
	}
	
	// Suppression d'un marin 
	public boolean removeMarin(Marin nouveauMarin)
	{
		boolean bool;
		bool = collectionMarin.remove(nouveauMarin);
		return bool;
	}
	
	// Test de pr�sence d'un marin
	public boolean isMarinPresent(Marin nouveauMarin)
	{
		boolean bool;
		bool = collectionMarin.contains(nouveauMarin);
		return bool;
	}
	
	@Override
	public String toString() {
		return "Equipage [collectionMarin=" + collectionMarin + "]";
	}
	
	// Copier le contenu d'un �quipage dans l'�quipage courant
	public void addAllEquipage(Equipage equipage)
	{
		// Pour pouvoir parcourir la collectionMarin de equipage et non de l'equipage courant
		Collection<Marin> nouvelleCollectionMarin = equipage.collectionMarin;
		// Interface Iterator : it�rateur qui parcoure equipage
		Iterator<Marin> iter = nouvelleCollectionMarin.iterator();
		while(iter.hasNext())	// Tant que nous ne sommes pas � la fin de la collection
		{
			Marin marin = iter.next();		// Element suivant de la collection
			if(this.isMarinPresent(marin) == false)
			{
				this.addMarin(marin);
			}
		}
	}
	
	// Effacer le contenu d'un �quipage
	public void clear()
	{
		Iterator<Marin> iter = collectionMarin.iterator();
		while(iter.hasNext())
		{
			iter.next();
			iter.remove();
		}
	}
	
	// Avoir le nombre de marin d'un �quipage
	public int getNombreMarins()
	{
		int nbrMarin = 0;
		Iterator<Marin> iter = collectionMarin.iterator();
		while(iter.hasNext())
		{
			iter.next();
			nbrMarin++;
		}
		
		return nbrMarin;
	}
	
	// Salaire moyen des marins d'un equipage
	public int getMoyenneSalaire()
	{
		int nbrMarin = 0;
		int salaireMoyen = 0;
		Iterator<Marin> iter = collectionMarin.iterator();
		while(iter.hasNext())
		{
			Marin marin = iter.next();
			salaireMoyen = salaireMoyen + marin.getSalaire();
		}
		nbrMarin = getNombreMarins();
		salaireMoyen = (salaireMoyen / nbrMarin);
		
		return salaireMoyen;
	}
	
}