package org.mohammad.serie03.exo05;

public class Marin
{
	private String nom;
	private String prenom;
	private int naissance;
	
	public Marin(String nouveauNom, String nouveauPrenom, int nouvelleNaissance) {
		this.nom = nouveauNom;
		this.prenom = nouveauPrenom;
		this.naissance = nouvelleNaissance;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public int getNaissance() {
		return naissance;
	}

	public void setNaissance(int naissance) {
		this.naissance = naissance;
	}

	@Override
	public String toString() {
		return "Marin [nom=" + nom + ", prenom=" + prenom + ", naissance=" + naissance + "]";
	}
}