package org.mohammad.serie03.exo05;

import java.util.*;

public class RegistreMarin 
{
	private Map<Integer, ArrayList<Marin>> map = new HashMap<>();
	
	public void addMarin(Marin nouveauMarin)
	{
		int decennie = nouveauMarin.getNaissance() - nouveauMarin.getNaissance() % 10;
		ArrayList<Marin> listeMarins;
		
		if (map.get(decennie) == null) 
		{
			listeMarins = new ArrayList<>();
		} 
		else 
		{
			listeMarins = map.get(decennie);
		}
		listeMarins.add(nouveauMarin);
		
		map.put(decennie, listeMarins);
	}
	
	// Retourne la liste des marins d'une m�me decennie
	public List<Marin> get(int decennie)
	{
		return map.get(decennie);
	}
	
	public int count(int decennie)
	{
		if(this.get(decennie) == null)
		{
			return 0;
		}
		
		return this.get(decennie).size();
	}
	
	// Retour aux Collection et Iterator
	public int count()
	{
		Collection<ArrayList<Marin>> collectionValeurs = this.map.values();
		Iterator<ArrayList<Marin>> iter = collectionValeurs.iterator();
		int nbr = 0;
		
		while(iter.hasNext())
		{
			nbr = nbr + iter.next().size();
		}
		
		return nbr;
	}

	@Override
	public String toString() {
		return "RegistreMarin [map=" + map + "]";
	}
	
	
}
