package org.mohammad.serie03.exo05;

public class Main 
{
	public static void main(String[] args)
	{
		RegistreMarin registre = new RegistreMarin();
	
		Marin m1 = new Marin("Lamazou", "Titouan", 1955);
		Marin m2 = new Marin("Gautier", "Alain", 1962);
		Marin m3 = new Marin("Auguin", "Christophe", 1959);
		Marin m4 = new Marin("Le Cl�ac'h", "Armel", 1977);
		Marin m5 = new Marin("Desjoyeaux", "Michel", 1965);
		Marin m6 = new Marin("Riou", "Vincent", 1972);
		Marin m7 = new Marin("Gabart", "Fran�ois", 1983);
		
		registre.addMarin(m1);
		registre.addMarin(m2);
		registre.addMarin(m3);
		registre.addMarin(m4);
		registre.addMarin(m5);
		registre.addMarin(m6);
		registre.addMarin(m7);
		
		// Affichage des listes
		System.out.println("Affichage des tables de hachages par d�cennies : ");
		System.out.println("D�cennie 1950 : " + registre.get(1950));
		System.out.println("D�cennie 1960 : " + registre.get(1960));
		System.out.println("D�cennie 1970 : " + registre.get(1970));
		System.out.println("D�cennie 1980 : " + registre.get(1980));
		System.out.println("D�cennie 1980 : " + registre.get(1990));
		System.out.println("");
		
		// Nombre de marins par liste
		System.out.println("Nombre de marins par table de hachage : ");
		System.out.println("Nombre de marins 1950 : " + registre.count(1950));
		System.out.println("Nombre de marins 1960 : " + registre.count(1960));
		System.out.println("Nombre de marins 1970 : " + registre.count(1970));
		System.out.println("Nombre de marins 1980 : " + registre.count(1980));
		System.out.println("Nombre de marins 1980 : " + registre.count(1990));
		System.out.println("");
		
		// Nombre total de marins dans le registre
		System.out.println("Nombre de marins enrengistr�s dans le registre : " + registre.count() + "\n");
		
		
	}

}
