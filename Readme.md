R�ponses aux questions :

/********** S�rie 1 **********/

--> Exo 2
1/ Les m�thodes cr�es sont :
- Factorielle : Calcule la factorielle d'un nombre donn� de type int, double ou BigInteger.
- Main : Affiche les factorielles de 10, 13 et 25 avec diff�rents types en utilisant la classe Factorielle.

2/ Voici les r�sultats calcul�s par le programme :
- Factorielle de int 10 = 3628800
- Factorielle de double 10 = 3628800.0
- Factorielle de BigInteger 10 = 3628800
Pour l'instant, on ne remarque pas de grosses diff�rences entre les diff�rents types.

- Factorielle de int 13 = 1932053504
- Factorielle de double 13 = 6.2270208E9
- Factorielle de BigInteger 13 = 6227020800
Maintenant on commence � remarquer un �cart entre ces diff�rentes valeurs.
Le type int n'est plus appropri� pour de telles valeurs.

- Factorielle de int 25 = 2076180480
- Factorielle de double 25 = 1.5511210043330986E25
- Factorielle de BigInteger 25 = 15511210043330985984000000
On remarque maintenant que le type BigInteger permet d'avoir une valeur plus pr�cise !


--> Exo 3
1/ On cr�e une classe Bissextille qui prend en argument l'ann�e et retourne "true" si l'ann�e est bissextile
et "false" si l'ann�e n'est pas bissextile.

2/ Pour les ann�es donn�es en exemple : 
- 1900 n'est pas une ann�e bissextile
- 1901 n'est pas une ann�e bissextile
- 1996 est bien une ann�e bissextile
- 2000 est bien une ann�e bissextile


--> Exo 4
1/ 2/ On cr�e une classe Palindrome qui prend en argument un String et retourne "true" si la phrase est un
palindrome et "false" sinon.

3/ En testant les phrases donn�es dans la classe Main, elles sont bien toutes des palindromes.

La m�thode charAt de la classe String renvoie le caract�re correspondant � la position indiqu�e.
Exemple : Soit String mot = "Bonjour" alors mot.charAt(3) renvoie j.
L'�quivalent en C (grossi�rement) est mot[3] qui renvoie j.


/********** S�rie 2 **********/ 

--> Exo 5
1/ Premier constructeur : public Marin(String nouveauNom, String nouveauPrenom, int nouveauSalaire)

2/ Deuxi�me constructeur : public Marin(String nouveauNom, int nouveauSalaire)

3/ Pour cela deux m�thodes :
Soit on le fait � la main comme nous l'avons fait pour getNom() et setNom().

4/ Soit on g�n�re ces fonctions int�gr�s � Eclipse (beaucoup plus rapide et aussi efficace).
On fait pour cela Alt + Shift + S ==> Generate Getters and Setters
On l'a fait pour getPrenom, getSalaire, setNom et setSalaire.

5/ public void augmenteSalaire(int augmentation)

6/ Pour cr�er des objets d'une classe Marin :
Marin m1 = new Marin("Mohammad", "Akash",10500);

7/ Pour afficher directement les diff�rents champs avec System.out.println qui prend en argumant un String
nous allons g�n�rer dans la classe Marin (Alt+Shift+S) la fonction toString().

8/ Pour comparer deux instances, on va utiliser la m�thode equals().
On la g�n�re de la m�me fa�on. En la g�n�rant, on g�n�re �galement la m�thode hashCode().

9/ On peut faire : m1 = m2;

10/ Les codes de hachages obtenus sont : 
m1 : 933554165
m2 : 933554165
m3 : -58901893

On doit bien obtenir un code hachage identique pour m1 et m2, ces deux �tant identiques.
m3 �tant diff�rent de m1 et m2, le code de hachage doit �tre diff�rent ce qui est le cas.

11/ A) On va impl�menter � notre classe Marin l'interface Comparable<Marin> et cr�er la m�thode compareTo
qui va comparer les noms des marins puis les pr�noms si besoin.

B) En comparant plusieurs marins, on peut affirmer que le programme fonctionne correctement.


--> Exo 6
Dans cet exercice, on a import� la classe Marin, en faisant un copier-coller.
On cr�e un tableau de Marin qui contient 5 marins. 
Pour cr�er les diff�rentes m�thodes demand�es dans l'�nonc�, on va utiliser les Getters et les Setters cr��s pr�c�demment (getSalaire et setSalaire).

On teste ensuite ces m�thodes sur le tableau de Marin.
Les m�thodes fonctionnent correctement.

/********** S�rie 3 **********/

--> Exo 4
1/ Classe Equipage qui doit contenir une collection de Marin :
--> private Collection<Marin> collectionMarin = new ArrayList<Marin>();

2/ Voir Equipage.java.
Pour ces diff�rentes m�thodes, on utilise les m�thodes de Collection add(), remove() et contains().

3/ Alt + Shift + S pour g�n�rer cette m�thode.

4/ On teste les m�thodes cr��es pr�c�demment et elle fonctionnent correctement.

5/ Pour la m�thode addAllEquipage, qui consiste � copier le contenu d'un �quipage donn� en param�tre dans l'�quipage courant. On a donc besoin ici de parcourir la collection : 
Pour parcourir la collection, on utilise l'interface Iterator, un it�rateur monodorectionnel : 
Iterator<E> iter = collection.iterator(); avec E le type et collection le nom de la Collection.

Dans notre cas, nous voulons parcourir la collection de l'equipage fournit en param�tre donc :
// Pour pouvoir parcourir la collectionMarin de equipage et non de l'equipage courant
Collection<Marin> nouvelleCollectionMarin = equipage.collectionMarin;
Iterator<Marin> iter = nouvelleCollectionMarin.iterator();

Puis pour parcourir cette collection, on utilise les m�thodes de l'interface Iterator que sont hasnext() et next().
On v�rifie que le marin n'est pas dans l'�quipage et on l'ajoute dans l'equipage courant.

6/ 7/ 8/ Voir Equipage.java

9/ 10/ 11/ 
Nous voulons maintenant limiter le nombre de marins dans un equipage.
On ajoute alors le champs dans la classe Equipage : private int nombreMax;
On cr�e ensuite le constructeur et on modifie la m�thode addMarin() pour prendre en compte cette contrainte et enfin on teste dans la m�thode main().
Test de fonctionnement :
Dans l'�quipage 1, que nous avons limit� � 5 marins, contient 4 marins.
On ajoute un 5�me marin qui est bien ajout�.
On ajoute ensuite un 6�me marin mais il n'est pas ajout� car limite atteinte. 

--> Exo 5
1/ Nous voulons cr�er une classe RegistreMarin qui contient une table de hachage.
On cr�e cette table de hachage de type HashMap : private Map<Integer, ArrayList<Marin>> map = new HashMap<>();
Le premier param�tre (Integer) est le type de la cl� et le deuxi�me param�tre (ArrayList<Marin>) est le type des valeurs.

2/ Pour ajouter un marin � la table de hachage, on utilise la m�thode put(key, value).

3/ Pour retourner une liste de marins d'une associ� � une certaine cl�, on utise la m�thode get(key).

4/ On utilise encore get(key) tout en incr�mentant un compteur.

5/ Pour cela, on r�utilise les notions de collections et it�rateurs.

6/

7/ Les fonctions test�s fonctiennent correctement.  